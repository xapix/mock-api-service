<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<wsdl:definitions xmlns:soap="http://schemas.xmlsoap.org/wsdl/soap/" xmlns:tns="http://www.example.org/Pokemon/" xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" name="Pokemon" targetNamespace="http://www.example.org/Pokemon/">
  <wsdl:types>
    <xsd:schema targetNamespace="http://www.example.org/Pokemon/">
      <xsd:element name="GetByIdOperation">
        <xsd:complexType>
          <xsd:sequence>
          	<xsd:element name="id" type="xsd:int" />
          </xsd:sequence>
        </xsd:complexType>
      </xsd:element>
      <xsd:element name="GetByIdOperationResponse">
        <xsd:complexType>
          <xsd:sequence>
          	<xsd:element name="id" type="xsd:int" />
          	<xsd:element name="name" type="xsd:string"></xsd:element>
          	<xsd:element name="height" type="xsd:float"></xsd:element>
          	<xsd:element name="weight" type="xsd:float"></xsd:element>
          	<xsd:element name="ability" type="tns:pokemonAbility" maxOccurs="unbounded" minOccurs="0"></xsd:element>
          </xsd:sequence>
		</xsd:complexType>
      </xsd:element>
      <xsd:complexType name="pokemonAbility">
      	<xsd:sequence>
      		<xsd:element name="name" type="xsd:string"></xsd:element>
      	</xsd:sequence>
      </xsd:complexType>
    </xsd:schema>
  </wsdl:types>
  <wsdl:message name="GetByIdOperationRequest">
    <wsdl:part element="tns:GetByIdOperation" name="parameters"/>
  </wsdl:message>
  <wsdl:message name="GetByIdOperationResponse">
    <wsdl:part element="tns:GetByIdOperationResponse" name="parameters"/>
  </wsdl:message>
  <wsdl:portType name="Pokemon">
    <wsdl:operation name="GetByIdOperation">
      <wsdl:input message="tns:GetByIdOperationRequest"/>
      <wsdl:output message="tns:GetByIdOperationResponse"/>
    </wsdl:operation>
  </wsdl:portType>
  <wsdl:binding name="PokemonSOAP" type="tns:Pokemon">
    <soap:binding style="document" transport="http://schemas.xmlsoap.org/soap/http"/>
    <wsdl:operation name="GetByIdOperation">
      <soap:operation soapAction="http://www.example.org/Pokemon/GetByIdOperation"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
  </wsdl:binding>
  <wsdl:service name="Pokemon">
    <wsdl:port binding="tns:PokemonSOAP" name="PokemonSOAP">
      <soap:address location="http://www.example.org/"/>
    </wsdl:port>
  </wsdl:service>
</wsdl:definitions>
